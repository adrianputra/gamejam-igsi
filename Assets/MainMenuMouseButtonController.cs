using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MainMenuMouseButtonController : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    private bool mouse_over = false;
    private RectTransform rTransform;

    void Awake()
    {
        rTransform = GetComponent<RectTransform>();
    }

    void Update()
    {
        if (mouse_over)
        {
            Debug.Log("Mouse Over");
            rTransform.localScale = Vector3.one * 1.05f;
        } else
        {
            rTransform.localScale = Vector3.one;
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        mouse_over = true;
        Debug.Log("Mouse enter");
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        mouse_over = false;
        Debug.Log("Mouse exit");
    }
}
