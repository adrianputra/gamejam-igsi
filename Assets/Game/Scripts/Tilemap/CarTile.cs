using UnityEngine;
using UnityEngine.Tilemaps;

[CreateAssetMenu(fileName = "carTile_", menuName = "Tiles/CarTile")]
public class CarTile : Tile
{
    public GameObject prefab;
    public Direction direction = Direction.Front;
    //public override bool StartUp(Vector3Int position, ITilemap tilemap, GameObject go)
    //{
    //    if (!go) return false;
    //    CarController carController = go.GetComponent<CarController>();
    //    carController.Position = position;
    //    carController.OldPosition = position;
    //    carController.Direction = direction;
    //    carController.UpdateSprite();
    //    return true;
    //}
}
