using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class LevelTilemap : MonoBehaviour
{
    public Tilemap baseTilemap;

    Dictionary<string, Trigger> triggers = new Dictionary<string, Trigger>();
    Dictionary<string, List<Blocker>> blockers = new Dictionary<string, List<Blocker>>();

    public void RegisterTrigger(Trigger trigger)
    {
        triggers.Add(trigger.key, trigger);
    }

    public Trigger GetTrigger(string key)
    {
        if (triggers.TryGetValue(key, out Trigger trigger))
        {
            return trigger;
        }
        else
        {
            return null;
        }
    }

    public void RegisterBlocker(Blocker blocker)
    {
        if (blockers.TryGetValue(blocker.key, out List<Blocker> blockerList))
        {
            blockerList.Add(blocker);
        }
        else
        {
            blockers.Add(blocker.key, new List<Blocker>{ blocker });
        }
    }

    public List<Blocker> GetBlockerList(string key)
    {
        if (blockers.TryGetValue(key, out List<Blocker> blockerList))
        {
            return blockerList;
        }
        else
        {
            return new List<Blocker>();
        }
    }
}
