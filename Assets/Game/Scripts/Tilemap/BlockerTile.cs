using UnityEngine;
using UnityEngine.Tilemaps;

[CreateAssetMenu(fileName = "blockerTile_", menuName = "Tiles/BlockerTile")]
public class BlockerTile : Tile
{
    public GameObject prefab;
    public string key;

    //public override bool StartUp(Vector3Int position, ITilemap tilemap, GameObject go)
    //{
    //    var levelTilemap = tilemap.GetComponent<LevelTilemap>();
    //    if (!levelTilemap) return false;
    //    if (!go) return false;
    //    var blocker = go.GetComponent<Blocker>();
    //    blocker.key = key;
    //    var spriteRenderer = blocker.GetComponent<SpriteRenderer>();
    //    spriteRenderer.color = color;
    //    blocker.UpdateSprite();
    //    levelTilemap.RegisterBlocker(blocker);
    //    return true;
    //}
}
