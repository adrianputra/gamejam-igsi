using UnityEngine;
using UnityEngine.Tilemaps;

[CreateAssetMenu(fileName = "triggerTile_", menuName = "Tiles/TriggerTile")]
public class TriggerTile : Tile
{
    public GameObject prefab;
    public string key;

//    public override bool StartUp(Vector3Int position, ITilemap tilemap, GameObject go)
//    {
//        var levelTilemap = tilemap.GetComponent<LevelTilemap>();
//        if (!levelTilemap) return false;
//        if (!go) return false;
//        var trigger = go.GetComponent<Trigger>();
//        trigger.key = key;
//        var spriteRenderer = trigger.GetComponent<SpriteRenderer>();
//        spriteRenderer.color = color;
//        trigger.UpdateSprite();
//        levelTilemap.RegisterTrigger(trigger);
//        return true;
//    }
}
