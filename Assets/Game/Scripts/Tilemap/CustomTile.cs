using UnityEngine;
using UnityEngine.Tilemaps;

[CreateAssetMenu(fileName = "tile_", menuName = "Tiles/CustomTile")]
public class CustomTile : Tile
{
    public GameObject prefab;
}
