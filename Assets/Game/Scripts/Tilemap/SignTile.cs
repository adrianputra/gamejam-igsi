using UnityEngine;
using UnityEngine.Tilemaps;

[CreateAssetMenu(fileName = "signTile_", menuName = "Tiles/SignTile")]
public class SignTile : Tile
{
    public bool interactive = true;
    public Direction direction = Direction.Front;

    //public override bool StartUp(Vector3Int position, ITilemap tilemap, GameObject go)
    //{
    //    var levelTilemap = tilemap.GetComponent<LevelTilemap>();
    //    if (!levelTilemap) return false;
    //    var baseTilemap = levelTilemap.baseTilemap;
    //    var obj = baseTilemap.GetInstantiatedObject(position);
    //    if (!obj) return false;
    //    var signPost = obj.GetComponent<SignPost>();
    //    signPost.automatic = false;
    //    signPost.interactive = interactive;
    //    signPost.SetDirection(direction);
    //    return true;
    //}
}
