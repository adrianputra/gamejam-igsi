using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public enum Direction
{
    Front, Right, Back, Left, Count
}

public enum TileType
{
    SignPost,
    Turn
}

public class LevelManager : MonoBehaviour
{
    const float PADDING = 1.2f;
    
    [SerializeField] Transform ContainerLevel;
    public Vector3 Offset = new Vector3(0, 0.35f, 0);

    Dictionary<Vector3Int, SignPost> signPosts = new Dictionary<Vector3Int, SignPost>();
    Dictionary<Vector3Int, Trigger> triggers = new Dictionary<Vector3Int, Trigger>();
    Dictionary<Vector3Int, Blocker> blockers = new Dictionary<Vector3Int, Blocker>();
    Dictionary<string, Trigger> triggersByKey = new Dictionary<string, Trigger>();
    Dictionary<string, List<Blocker>> blockersByKey = new Dictionary<string, List<Blocker>>();
    List<CarController> cars;

    [HideInInspector] public Tilemap tilemapBase;
    [HideInInspector] public Tilemap tilemapCar;
    [HideInInspector] public Tilemap tilemapLevel;
    [HideInInspector] public Grid grid;

    int _signPostCount;
    int _turnCount;
    public float Timer;

    GameManager _gameManager;
    UIManager _uiManager;
    bool _levelReady;

    LevelWrapper _currentLevel;
    Vector3Int gridSize;

    public void Init(GameManager gameManager)
    {
        _gameManager = gameManager;
    }
    
    void LoadLevel()
    {
        int levelIndex = Math.Min(_gameManager.UserData.Level, _gameManager.MaxLevel);
        _currentLevel = Instantiate(_gameManager.LevelInfos[levelIndex].LevelWrapper, ContainerLevel);

        signPosts = new Dictionary<Vector3Int, SignPost>();
        triggers = new Dictionary<Vector3Int, Trigger>();
        blockers = new Dictionary<Vector3Int, Blocker>();
        triggersByKey = new Dictionary<string, Trigger>();
        blockersByKey = new Dictionary<string, List<Blocker>>();
        cars = new List<CarController>();

        tilemapBase = _currentLevel.TilemapBase;
        tilemapCar = _currentLevel.TilemapCar;
        tilemapLevel = _currentLevel.TilemapLevel;
        grid = tilemapBase.layoutGrid;

        gridSize = _currentLevel.GridSize.ConvertToVector3Int();
        
        InstantiateSignPost();
        InstantiateCar();
        InstantiateLevel();

        //for(int i = 0; i < _carCount; i++)
        //{
        //    cars[i].Init(this);
        //}
        
        SetCameraSize();

        Timer = 0;
        _levelReady = true;
    }

    private void InstantiateSignPost()
    {
        for (var i = 0; i < gridSize.x; i++)
        {
            for (var j = 0; j < gridSize.y; j++)
            {
                var pos = new Vector3Int(i, j, 0);
                var tile = tilemapBase.GetTile<CustomTile>(pos);
                if (tile)
                {
                    var signPost = Instantiate(tile.prefab, grid.GetCellCenterWorld(pos), Quaternion.identity, tilemapBase.transform).GetComponent<SignPost>();
                    signPosts.Add(pos, signPost);
                }
            }
        }
    }

    private void InstantiateCar()
    {
        for (var i = 0; i < gridSize.x; i++)
        {
            for (var j = 0; j < gridSize.y; j++)
            {
                var pos = new Vector3Int(i, j, 0);
                var tile = tilemapCar.GetTile<CarTile>(pos);
                if (tile)
                {
                    var car = Instantiate(tile.prefab, grid.GetCellCenterWorld(pos), Quaternion.identity, tilemapCar.transform).GetComponent<CarController>();
                    car.Position = pos;
                    car.OldPosition = pos;
                    car.Direction = tile.direction;
                    car.Init(this);
                    cars.Add(car);
                }
            }
        }
    }

    private void InstantiateLevel()
    {
        for (var i = 0; i < gridSize.x; i++)
        {
            for (var j = 0; j < gridSize.y; j++)
            {
                var pos = new Vector3Int(i, j, 0);

                var triggerTile = tilemapLevel.GetTile<TriggerTile>(pos);
                if (triggerTile)
                {
                    var trigger = Instantiate(triggerTile.prefab, grid.GetCellCenterWorld(pos), Quaternion.identity, tilemapLevel.transform).GetComponent<Trigger>();
                    trigger.key = triggerTile.key;
                    var spriteRenderer = trigger.GetComponent<SpriteRenderer>();
                    spriteRenderer.color = triggerTile.color;
                    trigger.UpdateSprite();
                    triggers.Add(pos, trigger);
                    triggersByKey.Add(trigger.key, trigger);
                }

                var blockerTile = tilemapLevel.GetTile<BlockerTile>(pos);
                if (blockerTile)
                {
                    var blocker = Instantiate(blockerTile.prefab, grid.GetCellCenterWorld(pos), Quaternion.identity, tilemapLevel.transform).GetComponent<Blocker>();
                    blocker.key = blockerTile.key;
                    var spriteRenderer = blocker.GetComponent<SpriteRenderer>();
                    spriteRenderer.color = blockerTile.color;
                    blocker.UpdateSprite();
                    blockers.Add(pos, blocker);
                    if (blockersByKey.TryGetValue(blocker.key, out var blockerList))
                    {
                        blockerList.Add(blocker);
                    }
                    else
                    {
                        blockersByKey.Add(blocker.key, new List<Blocker> { blocker });
                    }
                }

                var signTile = tilemapLevel.GetTile<SignTile>(pos);
                if (signTile)
                {
                    if (signPosts.TryGetValue(pos, out var signPost)) {
                        signPost.automatic = false;
                        signPost.interactive = signTile.interactive;
                        signPost.SetDirection(signTile.direction);
                    }
                }
            }
        }
    }

    public void LoadNextLevel()
    {
        int nextLevel = Math.Min(_gameManager.UserData.Level + 1, _gameManager.MaxLevel);
        _gameManager.UserData.Level = nextLevel;
        PrepareLevel();
    }

    public void ClearLevel()
    {
        _levelReady = false;
        if(_currentLevel != null)
        {
            DestroyImmediate(_currentLevel.gameObject);
            _currentLevel = null;
        }
    }
    
    public void PrepareLevel()
    {
        ClearLevel();
        LoadLevel();
        _gameManager.ChangeState(GameState.Preparation);
    }

    // Update is called once per frame
    public void DoUpdate(float dt)
    {
        if(_levelReady && _gameManager.State == GameState.Gameplay)
        {
            Timer += dt * 1000;
            foreach (var car in cars)
            {
                car.DoUpdate(dt);
            }
            
            CheckWinLose();
        }
    }

    public void UpdateBlocker(Trigger trigger)
    {
        if (blockersByKey.TryGetValue(trigger.key, out var blockerList))
        {
            foreach (var blocker in blockerList)
            {
                blocker.SetLock(trigger.GetLock());
            }
        }
    }

    public void EnterTile(Vector3Int pos)
    {
        if (triggers.TryGetValue(pos, out var trigger))
        {
            trigger.DoPress();

            UpdateBlocker(trigger);
        }
    }

    public void ExitTile(Vector3Int pos)
    {
        if (triggers.TryGetValue(pos, out var trigger))
        {
            trigger.DoUnpress();

            UpdateBlocker(trigger);
        }
    }

    public bool IsBlocking(Vector3Int pos, Direction enterDirection)
    {
        // if there's road
        if (signPosts.TryGetValue(pos, out var signPost))
        {
            // there's road, check the available direction
            if (!signPost.AvailableDirections.Contains(enterDirection.Reverse())) return true;

            // check Blocker
            if (blockers.TryGetValue(pos, out var blocker))
            {
                return blocker.GetLock();
            }

            return false;
        }

        // if there's no road, then it's blocking
        return true;
    }

    public Direction GetDirection(CarController car, bool random = false)
    {
        Direction direction = car.Direction;

        if (signPosts.TryGetValue(car.Position, out var signPost))
        {
            var invalidDirections = new List<Direction>();
            for (var i = 0; i < (int)Direction.Count; i++)
            {
                var dir = (Direction)i;
                if (IsBlocking(car.Position + dir.ConvertToVector(), dir))
                {
                    invalidDirections.Add(dir);
                }
            }
            direction = signPost.GetDirection(car.Direction, invalidDirections, random);
        }

        return direction;
    }
    void CheckWinLose()
    {
        // cek di tile yang sama duluan
        for (var i = 0; i < cars.Count; i++)
        {
            var carA = cars[i];
            for (var j = i + 1; j < cars.Count; j++)
            {
                var carB = cars[j];

                // on same tile
                var sameTile = carA.Position == carB.Position;

                if (sameTile)
                {
                    if (carA.GetType() == typeof(EnemyCar) || carB.GetType() == typeof(EnemyCar))
                    {
                        _gameManager.ChangeState(GameState.Win);
                    }
                    else
                    {
                        _gameManager.ChangeState(GameState.Lose);
                    }
                    return;
                }
            }
        }
        // kemudian check phasing pake target posisinya
        for (var i = 0; i < cars.Count; i++)
        {
            var carA = cars[i];
            for (var j = i + 1; j < cars.Count; j++)
            {
                var carB = cars[j];

                // phasing
                var phasing = carA._currentTarget == carB.Position && carB._currentTarget == carA.Position;

                if (phasing)
                {
                    if (carA.GetType() == typeof(EnemyCar) || carB.GetType() == typeof(EnemyCar))
                    {
                        _gameManager.ChangeState(GameState.Win);
                    }
                    else
                    {
                        _gameManager.ChangeState(GameState.Lose);
                    }
                    return;
                }
            }
        }

        //Vector3Int currPos = Vector3Int.zero;
        //for(int i = 0; i < gridSize.x; i++)
        //{
        //    for(int j = 0; j < gridSize.y; j++)
        //    {
        //        int policeCount = 0;
        //        int enemyCount = 0;
        //        currPos.x = i;
        //        currPos.y = j;
        //        for(int x = 0; x < _carCount; x++)
        //        {
        //            if(Cars[x].Position.Equals(currPos))
        //            {
        //                if(Cars[x].GetType() == typeof(EnemyCar))
        //                    enemyCount++;
        //                else
        //                    policeCount++;
        //            }

        //            if(policeCount >= 1 && enemyCount >= 1)
        //            {
        //                _gameManager.ChangeState(GameState.Win);
        //            } else if(policeCount >= 2)
        //            {
        //                _gameManager.ChangeState(GameState.Lose);
        //            }

        //            if(_gameManager.State != GameState.Gameplay)
        //                return;
        //        }
        //    }
        //}
    }
    
    void SetCameraSize()
    {
        Vector3 south = grid.CellToWorld(Vector3Int.zero);
        Vector3 west = grid.CellToWorld(new Vector3Int(0, gridSize.y, 0));
        Vector3 north = grid.CellToWorld(new Vector3Int(gridSize.x, gridSize.y, 0));
        Vector3 east = grid.CellToWorld(new Vector3Int(gridSize.x, 0, 0));

        Rect boundingBox = Rect.MinMaxRect(west.x - PADDING, south.y - PADDING, east.x + PADDING, north.y + PADDING);
        
        Camera camera = Camera.main; 
        float orthographicSize = camera.orthographicSize;
        Vector3 topRight = new Vector3(boundingBox.x + boundingBox.width, boundingBox.y, 0f);
        Vector3 topRightAsViewport = camera.WorldToViewportPoint(topRight);
       
        if (topRightAsViewport.x >= topRightAsViewport.y)
            orthographicSize = Mathf.Abs(boundingBox.width) / camera.aspect / 2f;
        else
            orthographicSize = Mathf.Abs(boundingBox.height) / 2f;

        camera.orthographicSize = orthographicSize;
        
        Vector3 camPos = new Vector3(boundingBox.center.x, boundingBox.center.y, 0);
        _gameManager.CamRig.position = camPos;
    }
}
