using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class UserData
{
    public int Level;
    public bool AudioState;
    // TODO another data to be saved

    public UserData()
    {
        Level = 1;
        AudioState = true;
    }
}

public enum GameState
{
    MainMenu,
    LevelSelection,
    Gameplay,
    Preparation,
    Pause,
    Win,
    Lose,
    Credits
}

[SerializeField]
public class LevelInfo
{
    public LevelWrapper LevelWrapper;
    public int LevelNumber;

    public LevelInfo(LevelWrapper level, int number)
    {
        LevelWrapper = level;
        LevelNumber = number;
    }
}

public class GameManager : MonoBehaviour
{
    const string KEY_USERDATA = "user_data";
    const float ORTHO_SIZE_DEFAULT = 6;

    public Transform CamRig; 
    public UIManager UIManager;
    public LevelManager LevelManager;
    
    [HideInInspector] public GameState State;
    [HideInInspector] public UserData UserData;
    [HideInInspector] public int MaxLevel;
    
    [Header("Debug")]
    [SerializeField] bool TestMode;

    [HideInInspector] public List<LevelInfo> LevelInfos;
    
    void Start()
    {
        LoadData();

        LevelWrapper[] levels = Resources.LoadAll<LevelWrapper>("Levels");
        LevelInfos = new List<LevelInfo>();
        for(int i = 0; i < levels.Length; i++)
        {
            int number;
            if (int.TryParse(levels[i].name.Replace("Level", ""), out number))
            {
                LevelInfo info = new LevelInfo(levels[i], number);
                LevelInfos.Add(info);
            }
        }

        MaxLevel = LevelInfos.Count;
        LevelInfos.Sort((x, y) => x.LevelNumber.CompareTo(y.LevelNumber));

        SoundManager.Instance.Init(this);
        UIManager.gameObject.SetActive(true);
        LevelManager.Init(this);
        UIManager.Init(this);

        GameState initState = TestMode ? GameState.LevelSelection : GameState.MainMenu;
        ChangeState(initState);
        SoundManager.Instance.PlayBGM();
    }

    void Update()
    {
        float dt = Time.deltaTime;
        
        // TODO update all managers here
        LevelManager.DoUpdate(dt);
        UIManager.DoUpdate(dt);
    }

    public void ChangeState(GameState state)
    {
        State = state;
        UIManager.ChangeState(state);
        
        if(state == GameState.MainMenu) ResetCamera();
    }

    public void LoadData()
    {
        if(PlayerPrefs.HasKey(KEY_USERDATA))
        {
            string saved = PlayerPrefs.GetString(KEY_USERDATA);
            UserData = JsonUtility.FromJson<UserData>(saved);
        }
        else
        {
            UserData = new UserData();
        }
    }

    public void SaveData()
    {
        PlayerPrefs.SetString(KEY_USERDATA, JsonUtility.ToJson(UserData));
        PlayerPrefs.Save();
    }

    public void ResetCamera()
    {
        CamRig.transform.position = Vector3.zero;
        Camera.main.orthographicSize = ORTHO_SIZE_DEFAULT;
    }
}
