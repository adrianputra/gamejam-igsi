using UnityEngine;

public class UIManager : MonoBehaviour
{
    // TODO put all UI screen/popup here
    [SerializeField] MainMenuUI MainMenuUI;
    [SerializeField] LevelSelectionUI LevelSelectionUI;
    [SerializeField] GameplayUI GameplayUI;
    [SerializeField] PreparationUI PreparationUI;
    [SerializeField] PauseUI PauseUI;
    [SerializeField] WinUI WinUI;
    [SerializeField] LoseUI LoseUI;
    [SerializeField] MainMenuWorldUI MainMenuWorldUI;
    [SerializeField] CreditsUI CreditsUI;
    [SerializeField] GameObject MonitorUI;

    GameManager _gameManager;
    BaseUI _currentUI;
    
    public void Init(GameManager gameManager)
    {
        _gameManager = gameManager;
        
        MainMenuUI.Init(gameManager);
        LevelSelectionUI.Init(gameManager);
        GameplayUI.Init(gameManager);
        PreparationUI.Init(gameManager);
        PauseUI.Init(gameManager);
        WinUI.Init(gameManager);
        LoseUI.Init(gameManager);
        CreditsUI.Init(gameManager);
        MainMenuWorldUI.Init(gameManager);
    }

    public void DoUpdate(float dt)
    {
        if(_currentUI != null)
        {
            _currentUI.DoUpdate(dt);
        }
    }

    public void ChangeState(GameState state)
    {
        if(_currentUI != null)
        {
            _currentUI.Hide();
        }

        MainMenuWorldUI.Hide();
        MonitorUI.SetActive(true);
        switch (state)
        {
            case GameState.MainMenu:
                _currentUI = MainMenuUI;
                MainMenuWorldUI.Show();
                MonitorUI.SetActive(false);
                break;

            case GameState.Credits:
                _currentUI = CreditsUI;
                MonitorUI.SetActive(false);
                break;

            case GameState.LevelSelection:
                _currentUI = LevelSelectionUI;
                break;
            
            case GameState.Gameplay:
                _currentUI = GameplayUI;
                break;
            
            case GameState.Preparation:
                _currentUI = PreparationUI;
                break;
            
            case GameState.Pause:
                _currentUI = PauseUI;
                break;
            
            case GameState.Win:
                _currentUI = WinUI;
                break;
            
            case GameState.Lose:
                _currentUI = LoseUI;
                break;
        }
        _currentUI.Show();
    }
}
