using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LevelItemUI : MonoBehaviour
{
    [SerializeField] Button ButtonLevel;
    [SerializeField] TextMeshProUGUI TextLevelName;
    [SerializeField] TextMeshProUGUI TextLevel;

    GameManager _gameManager;
    LevelManager _levelManager;
    int _level;
    
    public void Init(GameManager gameManager, int levelIndex)
    {
        _gameManager = gameManager;
        _levelManager = gameManager.LevelManager;
        _level = levelIndex;

        LevelInfo info = gameManager.LevelInfos[levelIndex];
        TextLevel.SetText(info.LevelNumber.ToString("00"));
        TextLevelName.SetText(info.LevelWrapper.LevelName);
        ButtonLevel.onClick.AddListener(OnClickButtonLevel);
    }

    void OnClickButtonLevel()
    {
        _gameManager.UserData.Level = _level;
        _levelManager.PrepareLevel();
    }
}
