using UnityEngine;
using UnityEngine.UI;

public class PreparationUI : BaseUI
{
    [SerializeField] Button ButtonPlay;

    public override void Init(GameManager gameManager)
    {
        base.Init(gameManager);
        
        ButtonPlay.onClick.AddListener(OnClickButtonPlay);
    }

    public override void DoUpdate(float dt)
    {
        bool pause = Input.GetKeyDown(KeyCode.Space);
        if (pause)
        {
            GameManager.ChangeState(GameState.Gameplay);
        }
    }

    void OnClickButtonPlay()
    {
        GameManager.ChangeState(GameState.Gameplay);
    }
}
