using UnityEngine;
using UnityEngine.UI;

public class LoseUI : BaseUI
{
    [SerializeField] Button OverlayButton;
    [SerializeField] Button ButtonRetry;
    
    public override void Init(GameManager gameManager)
    {
        base.Init(gameManager);

        OverlayButton.onClick.AddListener(OnClickButtonRetry);
        ButtonRetry.onClick.AddListener(OnClickButtonRetry);
    }

    public override void Show()
    {
        base.Show();
        SoundManager.Instance.PlaySfx(SFX.Lose);
    }

    public override void DoUpdate(float dt)
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            OnClickButtonRetry();
        }
    }

    void OnClickButtonRetry()
    {
        GameManager.LevelManager.PrepareLevel();
        SoundManager.Instance.PlaySfx(SFX.Button);
    }
}
