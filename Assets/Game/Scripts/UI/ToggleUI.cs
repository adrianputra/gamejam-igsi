using UnityEngine;
using UnityEngine.UI;

public class ToggleUI : MonoBehaviour
{
    [SerializeField] Sprite SpriteOn;
    [SerializeField] Sprite SpriteOff;
    [SerializeField] Image Icon;

    public void SetState(bool state)
    {
        Icon.sprite = state ? SpriteOn : SpriteOff;
    }
}
