using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseUI : MonoBehaviour
{
    [HideInInspector] public GameManager GameManager;
    [HideInInspector] public UIManager UIManager;
    
    public virtual void Init(GameManager gameManager)
    {
        GameManager = gameManager;
        UIManager = gameManager.UIManager;
        
        gameObject.SetActive(false);
    }

    public virtual void Show()
    {
        gameObject.SetActive(true);
    }

    public virtual void Hide()
    {
        gameObject.SetActive(false);
    }

    public virtual void DoUpdate(float dt) {}
}
