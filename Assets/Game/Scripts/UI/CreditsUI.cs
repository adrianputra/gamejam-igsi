using UnityEngine;
using UnityEngine.UI;

public class CreditsUI : BaseUI
{
    [SerializeField] Button ButtonBack;

    public override void Init(GameManager gameManager)
    {
        base.Init(gameManager);

        ButtonBack.onClick.AddListener(OnClickButtonBack);
    }

    void OnClickButtonBack()
    {
        GameManager.ChangeState(GameState.MainMenu);
        SoundManager.Instance.PlaySfx(SFX.Button);
    }
}
