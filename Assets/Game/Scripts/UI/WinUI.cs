using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class WinUI : BaseUI
{
    [SerializeField] TextMeshProUGUI TextTimer;
    [SerializeField] Button OverlayButton;
    [SerializeField] Button ButtonNextLevel;
    
    public override void Init(GameManager gameManager)
    {
        base.Init(gameManager);

        OverlayButton.onClick.AddListener(OnClickButtonNext);
        ButtonNextLevel.onClick.AddListener(OnClickButtonNext);
    }

    public override void Show()
    {
        base.Show();
        
        TextTimer.SetText(GameManager.LevelManager.Timer.ConvertToTimeString(monoSpace: false));
        SoundManager.Instance.PlaySfx(SFX.Win);
    }

    public override void DoUpdate(float dt)
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            OnClickButtonNext();
        }
    }

    void OnClickButtonNext()
    {
        GameManager.LevelManager.LoadNextLevel();
        SoundManager.Instance.PlaySfx(SFX.Button);
    }
}
