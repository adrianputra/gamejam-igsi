using UnityEngine;
using UnityEngine.UI;

public class MainMenuUI : BaseUI
{
    [SerializeField] Button ButtonStart;
    [SerializeField] Button ButtonCredits;
    [SerializeField] Button ButtonQuit;

    public override void Init(GameManager gameManager)
    {
        base.Init(gameManager);
        
        ButtonStart.onClick.AddListener(OnClickButtonStart);
        ButtonCredits.onClick.AddListener(OnClickButtonCredits);
        ButtonQuit.onClick.AddListener(OnClickButtonQuit);
    }

    void OnClickButtonStart()
    {
        GameManager.ChangeState(GameState.LevelSelection);
        SoundManager.Instance.PlaySfx(SFX.Button);
    }

    void OnClickButtonCredits()
    {
        SoundManager.Instance.PlaySfx(SFX.Button);
        GameManager.ChangeState(GameState.Credits);
    }

    void OnClickButtonQuit()
    {
        SoundManager.Instance.PlaySfx(SFX.Button);
        Application.Quit();
    }
}
