using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelSelectionUI : BaseUI
{
    [SerializeField] Transform ContainerItem;
    [SerializeField] LevelItemUI PrefabLevelItem;
    
    [Header("Button")]
    [SerializeField] Button ButtonMainMenu;
    [SerializeField] Button ButtonToggleAudio;
    [SerializeField] ToggleUI ToggleAudio;

    List<LevelItemUI> _levelItemUIs;
    
    public override void Init(GameManager gameManager)
    {
        base.Init(gameManager);
        
        _levelItemUIs = new List<LevelItemUI>();
        for(int i = 0; i < GameManager.MaxLevel; i++)
        {
            LevelItemUI levelItem = Instantiate(PrefabLevelItem, ContainerItem);
            levelItem.Init(gameManager, i);
            _levelItemUIs.Add(levelItem);
        }
        
        ButtonMainMenu.onClick.AddListener(OnClickButtonMainMenu);
        ButtonToggleAudio.onClick.AddListener(OnClickToggleAudio);
    }

    public override void Show()
    {
        base.Show();
        
        ToggleAudio.SetState(GameManager.UserData.AudioState);
    }

    void OnClickButtonMainMenu()
    {
        GameManager.ChangeState(GameState.MainMenu);
    }
    
    void OnClickToggleAudio()
    {
        bool state = !GameManager.UserData.AudioState;
        GameManager.UserData.AudioState = state;
        SoundManager.Instance.ToggleAudio();
        ToggleAudio.SetState(state);
    }
}
