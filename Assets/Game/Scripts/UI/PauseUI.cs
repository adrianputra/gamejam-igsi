using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PauseUI : BaseUI
{
    [SerializeField] TextMeshProUGUI TextTitle;
    [SerializeField] TextMeshProUGUI TextTimer;
    
    [Header("Button")]
    [SerializeField] Button ButtonLevelSelection;
    [SerializeField] Button ButtonToggleAudio;
    [SerializeField] Button ButtonResume;

    [SerializeField] ToggleUI ToggleAudio;
    
    LevelManager _levelManager;

    public override void Init(GameManager gameManager)
    {
        base.Init(gameManager);
        _levelManager = gameManager.LevelManager;
        
        ButtonLevelSelection.onClick.AddListener(OnClickButtonLevelSelect);
        ButtonToggleAudio.onClick.AddListener(OnClickToggleAudio);
        ButtonResume.onClick.AddListener(OnClickButtonResume);
    }

    public override void Show()
    {
        base.Show();
        
        string title = string.Format("Cam {0}", GameManager.UserData.Level.ToString("00"));
        TextTitle.SetText(title);
        TextTimer.SetText(_levelManager.Timer.ConvertToTimeString());
        
        ToggleAudio.SetState(GameManager.UserData.AudioState);
    }

    public override void DoUpdate(float dt)
    {
        bool resume = Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Space);
        if(resume)
        {
            GameManager.ChangeState(GameState.Gameplay);
        }
    }
    
    void OnClickButtonLevelSelect()
    {
        GameManager.ChangeState(GameState.LevelSelection);
        GameManager.LevelManager.ClearLevel();
    }

    void OnClickToggleAudio()
    {
        bool state = !GameManager.UserData.AudioState;
        GameManager.UserData.AudioState = state;
        SoundManager.Instance.ToggleAudio();
        ToggleAudio.SetState(state);
    }

    void OnClickButtonResume()
    {
        GameManager.ChangeState(GameState.Gameplay);
    }
}
