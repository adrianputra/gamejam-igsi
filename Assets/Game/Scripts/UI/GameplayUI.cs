using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameplayUI : BaseUI
{
    [SerializeField] TextMeshProUGUI TextTitle;
    [SerializeField] TextMeshProUGUI TextTimer;
    [SerializeField] TextMeshProUGUI TextLevelName;
    
    [Header("Button")]
    [SerializeField] Button ButtonLevelSelection;
    [SerializeField] Button ButtonToggleAudio;
    [SerializeField] Button ButtonPause;
    [SerializeField] Button ButtonRetry;

    [SerializeField] ToggleUI ToggleAudio;

    LevelManager _levelManager;

    public override void Init(GameManager gameManager)
    {
        base.Init(gameManager);
        
        _levelManager = gameManager.LevelManager;
        
        ButtonLevelSelection.onClick.AddListener(OnClickButtonLevelSelect);
        ButtonToggleAudio.onClick.AddListener(OnClickToggleAudio);
        ButtonPause.onClick.AddListener(OnClickButtonPause);
        ButtonRetry.onClick.AddListener(OnClickButtonRetry);
    }

    public override void Show()
    {
        base.Show();

        LevelInfo levelInfo = GameManager.LevelInfos[GameManager.UserData.Level];
        string title = string.Format("Cam {0}", levelInfo.LevelNumber.ToString("00"));
        TextTitle.SetText(title);
        TextLevelName.SetText(levelInfo.LevelWrapper.LevelName);
        
        ToggleAudio.SetState(GameManager.UserData.AudioState);
    }

    public override void DoUpdate(float dt)
    {
        TextTimer.SetText(_levelManager.Timer.ConvertToTimeString());

        bool pause = Input.GetKeyDown(KeyCode.Space);
        if(pause)
        {
            GameManager.ChangeState(GameState.Pause);
        }
    }

    void OnClickButtonLevelSelect()
    {
        GameManager.ChangeState(GameState.LevelSelection);
        GameManager.LevelManager.ClearLevel();
    }

    void OnClickToggleAudio()
    {
        bool state = !GameManager.UserData.AudioState;
        GameManager.UserData.AudioState = state;
        SoundManager.Instance.ToggleAudio();
        ToggleAudio.SetState(state);
    }

    void OnClickButtonPause()
    {
        GameManager.ChangeState(GameState.Pause);
    }

    void OnClickButtonRetry()
    {
        GameManager.ChangeState(GameState.Preparation);
        GameManager.LevelManager.PrepareLevel();
        
    }
}
