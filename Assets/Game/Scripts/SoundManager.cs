﻿using System.Collections.Generic;
using UnityEngine;

public enum BGM
{
    Gameplay
}

public enum SFX
{
    Button,
    Signpost,
    TriggerPressed,
    CollidePolice,
    CollideEnemy,
    Win,
    Lose,
    Count
}

public class SoundManager
{
    public float globalVolume = 0.25f;
    const float BGM_VOLUME = 1f;
    const string BGM_PATH = "BGM/";
    const string SFX_PATH = "SFX/";
    
    static SoundManager _instance;
    public static SoundManager Instance
    {
        get
        {
            if(_instance == null)
            {
                _instance = new SoundManager();
            }

            return _instance;
        }
        
    }
    GameManager _gameManager;
    AudioSource _bgmSource;
    AudioSource[] _sfxSources;
    Dictionary<SFX, AudioSource> _sfxSourceDict;
    GameObject _audioSourceHolder;
    
    public void Init(GameManager gameManager)
    {
        this._gameManager = gameManager;
        
        _audioSourceHolder = new GameObject("Audio");
        InitBGM();
        InitSFX();
    }

    public void InitBGM()
    {
        _bgmSource = _audioSourceHolder.AddComponent<AudioSource>();
        _bgmSource.playOnAwake = true;
        _bgmSource.loop = true;
        _bgmSource.volume = BGM_VOLUME * globalVolume;
        _bgmSource.spatialBlend = 0;
        _bgmSource.clip = GetAudioClip(BGM_PATH + BGM.Gameplay);
    }

    public void InitSFX()
    {
        int sfxCount = (int)SFX.Count;
        
        _sfxSources = new AudioSource[sfxCount];
        _sfxSourceDict = new Dictionary<SFX, AudioSource>();
        
        for(int i = 0; i < sfxCount; i++)
        {
            _sfxSources[i] = _audioSourceHolder.AddComponent<AudioSource>();
            _sfxSources[i].playOnAwake = false;
            _sfxSources[i].loop = false;
            _sfxSources[i].spatialBlend = 0;
            _sfxSources[i].clip = GetAudioClip(SFX_PATH + ((SFX)i));

            _sfxSourceDict[(SFX)i] = _sfxSources[i];
        }
    }

    public void PlayBGM(float volume = 1, bool forcePlay = false)
    {
        if(_bgmSource.isPlaying &&
           !forcePlay)
        {
            return;
        }
        
        _bgmSource.volume = volume * globalVolume;
        _bgmSource.Play();
    }

    public void StopBGM()
    {
        _bgmSource.Stop();
    }
    
    public void PlaySfx(SFX sfx, float volume = 1)
    {
        AudioSource source = _sfxSourceDict[sfx];
        source.volume = volume * globalVolume;
        source.Play();
    }
    
    AudioClip GetAudioClip(string path)
    {
        AudioClip result = Resources.Load<AudioClip>("Audio/" + path);
        return result;
    }
    
    public void ToggleAudio()
    {
        bool isMute = !_gameManager.UserData.AudioState;
        int count = _sfxSources.Length;
        for(int i = 0; i < count; i++)
        {
            _sfxSources[i].mute = isMute;
        }

        _bgmSource.mute = isMute;
    }
}
