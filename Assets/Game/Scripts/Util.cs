using UnityEngine;

public static class Util
{
    public static Vector3Int ConvertToVector(this Direction direction)
    {
        Vector3Int vector = Vector3Int.zero;
        switch(direction)
        {
        case Direction.Front:
            vector = Vector3Int.up;
            break;
            
        case Direction.Right:
            vector = Vector3Int.right;
            break;
            
        case Direction.Back:
            vector = Vector3Int.down;
            break;
            
        case Direction.Left:
            vector = Vector3Int.left;
            break;
        }

        return vector;
    }

    public static Direction Reverse(this Direction direction)
    {
        switch (direction)
        {
            case Direction.Front:
                return Direction.Back;
            case Direction.Right:
                return Direction.Left;
            case Direction.Back:
                return Direction.Front;
            case Direction.Left:
                return Direction.Right;
            default:
                return Direction.Front;
        }
    }

    public static Direction TurnLeft(this Direction direction)
    {
        switch (direction)
        {
            case Direction.Front:
                return Direction.Left;
            case Direction.Right:
                return Direction.Front;
            case Direction.Back:
                return Direction.Right;
            case Direction.Left:
                return Direction.Back;
            default:
                return Direction.Front;
        }
    }

    public static Direction TurnRight(this Direction direction)
    {
        switch (direction)
        {
            case Direction.Front:
                return Direction.Right;
            case Direction.Right:
                return Direction.Back;
            case Direction.Back:
                return Direction.Left;
            case Direction.Left:
                return Direction.Front;
            default:
                return Direction.Front;
        }
    }

    public static Vector2Int ConvertToVector(this Vector3 pos)
    {
        Vector2Int vector = new Vector2Int((int)pos.x, (int)pos.y);
        return vector;
    }

    public static void DrawDebugLine(this Direction direction, Vector3 startPos)
    {
        Vector3Int dir = direction.ConvertToVector();
        Vector3 end = startPos + dir;
        Debug.DrawLine(startPos, end, Color.green);
    }

    public static Vector3Int ConvertToVector3Int(this Vector2Int cell)
    {
        Vector3Int vector = new Vector3Int(cell.x, cell.y, 0);
        return vector;
    }

    static int s_sec = 1000;
    static int s_min = 60 * s_sec;
    static int s_hour = 60 * s_min;
    public static string ConvertToTimeString(this float time, bool monoSpace = true)
    {
        int min = (int)time % s_hour / s_min;
        int sec = (int)time % s_hour % s_min / s_sec;
        int millis = (int)time % s_hour % s_min % s_sec / 10;

        string prefix = monoSpace ? "<mspace=40>" : "";
        string suffix = monoSpace ? "</mspace>" : "";
        string result = prefix + min.ToString("00") + ":" + sec.ToString("00") + ":" + millis.ToString("00") + suffix;
        return result;
    }
}
