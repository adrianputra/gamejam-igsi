using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCar : CarController
{
    public override void SetNextDestination()
    {
        // TODO enemy logic here
        int randomValue = Random.Range(0, 100);
        
        Direction = _levelManager.GetDirection(this, true);
        Vector3Int velocity = Direction.ConvertToVector();

        _currentTarget = Position + velocity;
        _currentPos = _grid.CellToWorld(Position) + _offset;
        _targetPos = _grid.CellToWorld(_currentTarget) + _offset;
        _lerpValue = 0;

        UpdateSprite();
    }
}
