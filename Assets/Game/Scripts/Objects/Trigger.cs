using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TriggerType
{
    Simple,
    Toggle,
    Push
}

public class Trigger : MonoBehaviour
{
    public string key;

    bool isLock = true;
    public TriggerType type = TriggerType.Simple;

    public Sprite lockSprite;
    public Sprite unlockSprite;

    SpriteRenderer spriteRenderer;

    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    // Start is called before the first frame update
    void Start()
    {
        UpdateSprite();
    }

    public void UpdateSprite()
    {
        if (isLock)
        {
            spriteRenderer.sprite = lockSprite;
            if (type == TriggerType.Simple)
            {
                spriteRenderer.sortingLayerName = "Objects";
            }
            else
            {
                spriteRenderer.sortingLayerName = "AboveMap";
            }
        }
        else
        {
            spriteRenderer.sprite = unlockSprite;
            spriteRenderer.sortingLayerName = "AboveMap";
        }
    }

    public void DoPress()
    {
        var oldIsLock = isLock;
        if (type == TriggerType.Toggle)
        {
            isLock = !isLock;
        }
        else
        {
            isLock = false;
        }
        UpdateSprite();
        if (isLock != oldIsLock)
        {
            SoundManager.Instance.PlaySfx(SFX.TriggerPressed);
        }
    }

    public void DoUnpress()
    {
        var oldIsLock = isLock;
        if (type == TriggerType.Push)
        {
            isLock = true;
        }
        UpdateSprite();
        if (isLock != oldIsLock)
        {
            SoundManager.Instance.PlaySfx(SFX.TriggerPressed);
        }
    }

    public bool GetLock()
    {
        return isLock;
    }
}
