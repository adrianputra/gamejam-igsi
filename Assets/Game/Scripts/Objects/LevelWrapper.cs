using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class LevelWrapper : MonoBehaviour
{
    public Tilemap TilemapBase;
    public Tilemap TilemapCar;
    public Tilemap TilemapLevel;
    public Vector2Int GridSize;
    public string LevelName;
}
