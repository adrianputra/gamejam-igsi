using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SignPost : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler
{
    public Vector3Int Position;
    public Direction Direction;

    public List<Direction> AvailableDirections;

    bool pressed = false;
    public bool automatic = false;
    public bool interactive = true;

    public List<Sprite> sprites;
    public List<Sprite> pressSprites;
    public List<Sprite> disabledSprites;
    SpriteRenderer spriteRenderer;
    
    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();

        pressed = false;
        UpdateSprite();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (automatic || !interactive) return;
        pressed = true;
        UpdateSprite();
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (automatic || !interactive) return;
        pressed = false;
        UpdateSprite();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (automatic || !interactive) return;
        int dirIndex = (int)Direction;
        Direction currDirection = Direction;
        do
        {
            dirIndex += 1;
            dirIndex = dirIndex % (int)Direction.Count;
            currDirection = (Direction)dirIndex;
        }
        while(!AvailableDirections.Contains(currDirection));

        Direction = currDirection;

        UpdateSprite();
        SoundManager.Instance.PlaySfx(SFX.Signpost);
    }

    public void UpdateSprite()
    {
        if (automatic)
        {
            spriteRenderer.enabled = false;
        }
        else
        {
            spriteRenderer.enabled = true;
        }

        if (!interactive)
        {
            spriteRenderer.sprite = disabledSprites[(int)Direction];
        } else if (pressed)
        {
            spriteRenderer.sprite = pressSprites[(int)Direction];
        }
        else
        {
            spriteRenderer.sprite = sprites[(int)Direction];
        }
    }

    public void SetDirection(Direction dir)
    {
        // if it's not allowed, don't change anything
        if (!AvailableDirections.Contains(dir)) return;

        Direction = dir;

        UpdateSprite();
    }

    public Direction GetDirection(Direction from, List<Direction> invalidDirections = null, bool random = false)
    {
        if (!automatic && !random)
        {
            if (!invalidDirections.Contains(Direction)) return Direction;
            else return from.Reverse();
        }
        else
        {
            // prioritise straight line (except if it's random, don't prioritise it)
            if (!random && AvailableDirections.Contains(from) && !invalidDirections.Contains(from))
            {
                return from;
            }

            // filter from Car Direction, and invalidDirection
            var dirs = AvailableDirections.FindAll(dir => 
                                                    dir != from.Reverse() && 
                                                    (invalidDirections == null || !invalidDirections.Contains(dir)));
            // no Available Direction, then go back
            if (dirs.Count == 0)
            {
                return from.Reverse();
            }
            return dirs[Random.Range(0, dirs.Count)];
        }
    }

    // TODO remove this if arrow is implemented
    void LateUpdate()
    {
        Direction.DrawDebugLine(transform.position);
    }
}
