using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarController : MonoBehaviour
{
    // TODO use scriptable object or other database
    [Header("Config")]
    public float MoveSpeed = 2f;
    public Direction Direction;

    protected LevelManager _levelManager;
    protected float _lerpValue;
    public Vector3Int Position;
    [HideInInspector] public Vector3Int OldPosition;
    [HideInInspector] public Vector3Int _currentTarget;
    protected Vector3 _currentPos;
    protected Vector3 _targetPos;

    protected Grid _grid;
    protected Vector3 _offset;
    bool _init;

    public List<Sprite> sprites;
    SpriteRenderer spriteRenderer;
    bool justEnterTile = true;

    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    public virtual void Init(LevelManager levelManager)
    {
        _levelManager = levelManager;
        _grid = levelManager.grid;
        _offset = levelManager.Offset;
        _lerpValue = 0f;

        SetDirection(Direction);
        _init = true;
    }

    // Update is called once per frame
    public virtual void DoUpdate(float dt)
    {
        if(_init)
        {
            if (justEnterTile)
            {
                _levelManager.EnterTile(_currentTarget); // need to update this next frame
                justEnterTile = false;
            }

            if (_lerpValue < 1f)
            {
                _lerpValue += dt * MoveSpeed;
                UpdatePosition();
            }
            else
            {
                OldPosition = Position;
                Position = _currentTarget;
                SetNextDestination();
                _levelManager.ExitTile(OldPosition);
                justEnterTile = true;
            }
        }
    }

    public virtual void SetNextDestination()
    {
        SetDirection(_levelManager.GetDirection(this));
    }

    public void SetDirection(Direction dir)
    {
        Direction = dir;
        Vector3Int velocity = Direction.ConvertToVector();

        _currentTarget = Position + velocity;
        _currentPos = _grid.CellToWorld(Position) + _offset;
        _targetPos = _grid.CellToWorld(_currentTarget) + _offset;
        _lerpValue = 0;

        UpdatePosition();
        UpdateSprite();
    }

    public void UpdatePosition()
    {
        transform.position = Vector3.Lerp(_currentPos, _targetPos, _lerpValue);
    }

    public void UpdateSprite()
    {
        spriteRenderer.sprite = sprites[(int)Direction];
    }
}
