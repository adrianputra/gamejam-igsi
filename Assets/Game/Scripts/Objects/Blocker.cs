using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blocker : MonoBehaviour
{
    public string key;

    bool isLock = true;
    public bool close = true;

    public Sprite lockSprite;
    public Sprite unlockSprite;

    SpriteRenderer spriteRenderer;

    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    // Start is called before the first frame update
    void Start()
    {
        isLock = close;
        UpdateSprite();
    }

    public void UpdateSprite()
    {
        if (isLock)
        {
            spriteRenderer.sprite = lockSprite;
            spriteRenderer.sortingLayerName = "Objects";
        }
        else
        {
            spriteRenderer.sprite = unlockSprite;
            spriteRenderer.sortingLayerName = "AboveMap";
        }
    }

    public void SetLock(bool state)
    {
        if (close == true)
        {
            isLock = state;
        }
        else
        {
            isLock = !state;
        }
        UpdateSprite();
    }

    public bool GetLock()
    {
        return isLock;
    }
}
