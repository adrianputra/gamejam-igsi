using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuBackgroundController : MonoBehaviour
{
    public Material bgMaterial;
    public float speed = 0.01f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 currOffset = bgMaterial.GetTextureOffset("_MainTex");

        Vector2 plus = Vector2.one;
        plus.x = -(speed * 0.001f);
        plus.y = (speed * 0.001f);

        bgMaterial.SetTextureOffset("_MainTex", currOffset + plus);
    }
}
