using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuTitleController : MonoBehaviour
{
    public Sprite []titles;
    public Image imageUI;

    public float time = 3;

    private float currentTime;
    private int indx = 0;

    // Start is called before the first frame update
    void Start()
    {
        currentTime = time;
    }

    // Update is called once per frame
    void Update()
    {
        currentTime -= Time.deltaTime;
        if (currentTime < 0)
        {
            indx = (indx + 1) % titles.Length;
            currentTime = time;
            imageUI.sprite = titles[indx];
        }
    }
}
