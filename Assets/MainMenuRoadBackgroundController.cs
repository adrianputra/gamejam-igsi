using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuRoadBackgroundController : MonoBehaviour
{
    public GameObject[] BG;
    public float speed = 1f;

    private Vector3 vecMove = new Vector3(-15.97f, 9.23f, 0);

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < BG.Length; i++)
        {
            BG[i].transform.localPosition += (vecMove.normalized * (Time.deltaTime * speed));
        }

        for (int i = 0; i < BG.Length; i++)
        {
            if (BG[i].transform.localPosition.x < -20)
            {
                GameObject obj = BG[i];
                for (int j = 0; j < BG.Length; j++)
                {
                    if (BG[j].transform.localPosition.x > obj.transform.localPosition.x)
                        obj = BG[j];
                }

                BG[i].transform.localPosition = obj.transform.localPosition + -(vecMove);
            }
        }
    }
}
